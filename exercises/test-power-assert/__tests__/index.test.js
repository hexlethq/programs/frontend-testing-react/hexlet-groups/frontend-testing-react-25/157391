const assert = require('power-assert');
const { flattenDepth } = require('lodash');

// BEGIN
    const array = [1, [2, [3]]]
    const flatArr = [1, 2, 3]
    const flatOneDepth = [1, 2, [3]]
    const zero = 0;
    const one = 1;
    const two = 2;
    const three = 3;
    const notInArray = 42;
    const minusOne = -1;

    const flattenArr = flattenDepth(array) // [1, 2, [3]]
    const flattenOneDepthArr = flattenDepth(array, 1) // [1, 2, [3]]

    assert(flattenArr.length === three);
    assert(flattenArr.indexOf(one) === zero);
    assert(flattenArr.indexOf(two) === one);
    assert(Array.isArray(flattenArr[two]));
    assert.ok(flattenArr.indexOf(notInArray) === minusOne, `${notInArray} Should not be in array`);
    assert.equal(Array.toString(flattenOneDepthArr), Array.toString(flatOneDepth))

    // Should return the same results for default and one depth as second parameter
    assert.equal(Array.toString(flattenOneDepthArr), Array.toString(flattenDepth(array)))

    // Should return flat array if second param is greater or equal to nested level
    assert.equal(Array.toString(flattenDepth(array, 2)), Array.toString(flatArr))
    assert.equal(Array.toString(flattenDepth(array, 5)), Array.toString(flatArr))

    // Should return the same array if invalid second parameter
    assert.equal(Array.toString(flattenDepth(array, 'invalid arg')), Array.toString(array))
// END
